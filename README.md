# TitleBar

## 简介
> TitleBar是基于ArkTS封装的HarmonyOS通用、功能全面的自定义标题栏框架。
>
![operation.gif](screenshots/operation.gif)

## 下载安装
```shell
ohpm install @smarthane/titlebar  
```

## 使用说明
1. 引入文件及代码依赖
 ```
    import { TitleBar } from '@smarthane/titlebar'
 ```
2. 使用步骤说明
 ```
   （1） 创建model
    @State model: TitleBar.Model = new TitleBar.Model()
   （2） 创建TitleBar
    TitleBar({ model: $model })
 ```
3. 使用示例

【详细可以参考[工程entry模块下面的示例代码](https://gitee.com/smarthane/titlebar/blob/master/entry/src/main/ets/pages/Index.ets)】

 ```

import { TitleBar } from '@smarthane/titlebar'
import promptAction from '@ohos.promptAction'

@Entry
@Component
struct Index {
  private swiperController: SwiperController = new SwiperController()
  private data: MyDataSource = new MyDataSource([])
  private scroller: Scroller = new Scroller()
  @State model1: TitleBar.Model = new TitleBar.Model()
    .setOnLeftClickListener(() => {
      promptAction.showToast({
        message: "点击了左边返回按钮",
        duration: 2000
      })
    })
    .setTitleName("默认标题栏")
  @State model2: TitleBar.Model = new TitleBar.Model()
    .setLeftIcon(null)
    .setTitleTextStyle(FontStyle.Italic)
    .setTitleName("不带返回按钮并且标题为斜体")
  @State model3: TitleBar.Model = new TitleBar.Model()
    .setTitleTextOverflowType(TitleBar.TitleTextOverflowType.MARQUEE)
    .setTitleName("标题栏文字过长跑马灯效果，标题栏文字过长跑马灯效果。")
    .setTitleFontColor(Color.Green)
  @State model4: TitleBar.Model = new TitleBar.Model()
    .setTitleName("主标题主标题文本超长时显示不下的文本用省略号代替")
    .setSubTitleName("副标题文本超长时显示不下的文本用省略号代替")
  @State model5: TitleBar.Model = new TitleBar.Model()
    .setTitleName("主副标题文字过长跑马灯效果。主副标题文字过长跑马灯效果。")
    .setTitleTextOverflowType(TitleBar.TitleTextOverflowType.MARQUEE)
    .setSubTitleName("副标题副标题副标题副标题副标题副标题副标题")
    .setSubTitleTextOverflowType(TitleBar.TitleTextOverflowType.MARQUEE)
  @State model6: TitleBar.Model = new TitleBar.Model()
    .setTitleName("主标题不动，副标题文字过长跑马灯效果。")
    .setSubTitleName("副标题副标题副标题副标题副标题副标题副标题")
    .setSubTitleFontColor(Color.Orange)
    .setSubTitleTextOverflowType(TitleBar.TitleTextOverflowType.MARQUEE)
  @State model7: TitleBar.Model = new TitleBar.Model()
    .setLeftTitleName("返回")
    .setTitleName("返回按钮文字")
  @State model8: TitleBar.Model = new TitleBar.Model()
    .setTitleName("标题标题标题")
    .setRightIcon($r("app.media.more_black"))
  @State model9: TitleBar.Model = new TitleBar.Model()
    .setLeftTitleName("扫描")
    .setLeftIcon($r("app.media.scan_black"))
    .setLeftIconGravity(TitleBar.IconGravity.TOP)
    .setLeftIconWidth(35)
    .setLeftIconHeight(35)
    .setLeftTitleFontSize(10)
    .setLeftTitleFontColor(Color.Blue)
    .setRightTitleName("打印")
    .setRightIcon($r("app.media.print_black"))
    .setRightIconGravity(TitleBar.IconGravity.TOP)
    .setRightIconWidth(35)
    .setRightIconHeight(35)
    .setRightTitleFontSize(10)
    .setTitleName("主标题可设置大小和颜色")
    .setTitleFontColor(Color.Red)
    .setOnLeftClickListener(() => {
      promptAction.showToast({
        message: "扫描",
        duration: 2000
      })
    })
    .setOnRightClickListener(() => {
      promptAction.showToast({
        message: "打印",
        duration: 2000
      })
    })
  @State model10: TitleBar.Model = new TitleBar.Model()
    .setTitleName("标题栏可以点击")
    .setOnTitleClickListener(() => {
      promptAction.showToast({
        message: "点击了标题栏",
        duration: 2000
      })
    })
  @State model11: TitleBar.Model = new TitleBar.Model()
    .setTitleName("自定义布局右边")
    .setOnTitleClickListener(() => {
      promptAction.showToast({
        message: "点击了标题栏",
        duration: 2000
      })
    })
  @State model12: TitleBar.Model = new TitleBar.Model()
    .setTitleName("自定义布局标题栏")
    .setRightIcon($r('app.media.more_black'))
  @State model13: TitleBar.Model = new TitleBar.Model()
    .setTitleName("自定义布局左边")
  @State model14: TitleBar.Model = new TitleBar.Model()
    .setTitleBarStyle(TitleBar.BarStyle.NIGHT)
    .setLeftTitleName("返回")
    .setRightTitleName("设置")
    .setOnLeftClickListener(() => {
      promptAction.showToast({
        message: "点击了左边返回按钮",
        duration: 2000
      })
    })
    .setTitleName("夜间模式标题栏")
  @State model15: TitleBar.Model = new TitleBar.Model()
    .setTitleBarStyle(TitleBar.BarStyle.LIGHT)
    .setLeftTitleName("返回")
    .setRightTitleName("设置")
    .setOnLeftClickListener(() => {
      promptAction.showToast({
        message: "点击了左边返回按钮",
        duration: 2000
      })
    })
    .setTitleName("白天模式标题栏")
  @State model16: TitleBar.Model = new TitleBar.Model()
    .setLeftIcon(null)
    .setLeftTitleName("返回")
    .setRightIcon(null)
    .setRightTitleName("设置")
    .setTitleName("没有左右图标的标题栏")
  @State model17: TitleBar.Model = new TitleBar.Model()
    .setTitleBarStyle(TitleBar.BarStyle.TRANSPARENT)
    .setLeftTitleName("返回")
    .setRightTitleName("设置")
    .setOnLeftClickListener(() => {
      promptAction.showToast({
        message: "点击了左边返回按钮",
        duration: 2000
      })
    })
    .setTitleName("透明标题栏")
  @State model18: TitleBar.Model = new TitleBar.Model()
    .setTitleBarStyle(TitleBar.BarStyle.LINEARGRADIENT)
    .setTitleBarLinearGradient({
      angle: 90,
      colors: [[0xff0000, 0.0], [0x0000ff, 0.3], [0xffff00, 1.0]]
    })
    .setLeftTitleName("返回")
    .setRightTitleName("设置")
    .setOnLeftClickListener(() => {
      promptAction.showToast({
        message: "点击了左边返回按钮",
        duration: 2000
      })
    })
    .setTitleName("渐变颜色标题栏")
  @State model19: TitleBar.Model = new TitleBar.Model()
    .setLeftTitleName("返回")
    .setRightTitleName("设置")
    .setTitleBarBottomLineHeight(0.5)
    .setTitleBarBottomLineColor(Color.Red)
    .setOnLeftClickListener(() => {
      promptAction.showToast({
        message: "点击了左边返回按钮",
        duration: 2000
      })
    })
    .setTitleName("设置底部横线")
  @State model20: TitleBar.Model = new TitleBar.Model()
    .setLeftTitleName("返回")
    .setRightTitleName("设置")
    .setTitleName("隐藏底部横线")
    .setShowTitleBarBottomLine(false)
  @State model21: TitleBar.Model = new TitleBar.Model()
    .setTitleBarStyle(TitleBar.BarStyle.LINEARGRADIENT)
    .setTitleBarLinearGradient({
      angle: 180,
      colors: [[0x26c4fd, 0.0], [0x387cfd, 1.0]]
    })
    .setLeftTitleName("返回")
    .setRightTitleName("确认")
    .setRightTitleFontColor(Color.Red)
    .setTitleName("仿QQ渐变颜色标题栏")
  // 垂直滚动标题自定义view实现方式
  @State model22: TitleBar.Model = new TitleBar.Model()
  @State model23: TitleBar.Model = new TitleBar.Model()
    .setTitleTextOverflowType(TitleBar.TitleTextOverflowType.MULTIPLE)
    .setMultipleTitleList(['1.垂直滚动标题实现方式', '2.这是一个垂直滚动的标题', '3.好用好用66好用好用好用好用好用好用'])
    .setMultipleTitleTextOverflowType(TitleBar.TitleTextOverflowType.ELLIPSIS)
    .setMultipleTitleFontColor(Color.Blue)
    .setMultipleTitleTextAlign(TextAlign.Start)
    .setMultipleTitlePlayInterval(2000)
    .setMultipleTitlePlayDuration(2600)
    .setMultipleTitlePlayItemSpace(10)
    .setRightTitleName("确认")

  @Builder customRightView() {
    Row({ space: 5 }) {
      Column({ space: 2 }) {
        Image($r('app.media.scan_black'))
          .objectFit(ImageFit.Auto)
          .width(30)
          .height(30)
        Text("扫描").fontSize(10)
      }

      Column({ space: 2 }) {
        Image($r('app.media.print_black'))
          .objectFit(ImageFit.Auto)
          .width(30)
          .height(30)
        Text("打印").fontSize(10)
      }

      Column({ space: 2 }) {
        Image($r('app.media.scan_black'))
          .objectFit(ImageFit.Auto)
          .width(30)
          .height(30)
        Text("333").fontSize(10)
      }
    }
    .padding(5)
  }

  @Builder customCenterView() {
    Row({ space: 5 }) {
      Column({ space: 2 }) {
        Image($r('app.media.scan_black'))
          .objectFit(ImageFit.Auto)
          .width(30)
          .height(30)
        Text("111").fontSize(10)
      }

      Column({ space: 2 }) {
        Image($r('app.media.print_black'))
          .objectFit(ImageFit.Auto)
          .width(30)
          .height(30)
        Text("222").fontSize(10)
      }

      Column({ space: 2 }) {
        Image($r('app.media.scan_black'))
          .objectFit(ImageFit.Auto)
          .width(30)
          .height(30)
        Text("333").fontSize(10)
      }
    }
    .justifyContent(FlexAlign.Center)
    .padding(5)
    .layoutWeight(1)
  }

  @Builder customCenterView2() {
    Row({ space: 5 }) {
      Swiper(this.swiperController) {
        LazyForEach(this.data, (item: string) => {
          Text(item)
            .textAlign(TextAlign.Center)
            .fontSize(15)
            .maxLines(1)
            .textOverflow({ overflow: TextOverflow.Ellipsis })
        }, item => item)
      }
      .autoPlay(true)
      .interval(2000)
      .indicator(false)
      .loop(true)
      .duration(2000)
      .itemSpace(5)
      .vertical(true)
      .curve(Curve.Linear)
    }
    .justifyContent(FlexAlign.Center)
    .padding(5)
    .layoutWeight(1)
  }

  build() {
    Stack({ alignContent: Alignment.TopStart }) {
      Scroll(this.scroller) {
        Flex({ direction: FlexDirection.Column }) {
          TitleBar({ model: $model1 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model2 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model3 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model4 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model5 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model6 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model7 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model8 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model9 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model10 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({
            model: $model11,
            customRightView: () => {
              this.customRightView()
            }
          })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({
            model: $model12,
            customCenterView: () => {
              this.customCenterView()
            }
          })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({
            model: $model13,
            customLeftView: () => {
              this.customRightView()
            }
          })
          TitleBar({ model: $model14 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model15 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model16 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model17 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model18 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model21 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model19 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model20 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({
            model: $model22,
            customCenterView: () => {
              this.customCenterView2()
            }
          })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
          TitleBar({ model: $model23 })
          Divider().strokeWidth(10).color(0xFFF1F0F0)
        }
      }
      .scrollable(ScrollDirection.Vertical)
      .scrollBar(BarState.Off)
      .edgeEffect(EdgeEffect.Spring)
    }
    .width('100%')
    .height('100%')
    .backgroundColor(0xFFF1F0F0)
  }

  aboutToAppear(): void {
    let list = []
    for (var i = 1; i <= 10; i++) {
      list.push(i.toString() + ".垂直滚动标题自定义view实现方式");
    }
    this.data = new MyDataSource(list)
  }
}

class MyDataSource implements IDataSource {
  private list: string[] = [];
  private listener: DataChangeListener;

  constructor(list: string[]) {
    this.list = list;
  }

  totalCount(): number {
    return this.list.length;
  }

  getData(index: number): any {
    return this.list[index];
  }

  registerDataChangeListener(listener: DataChangeListener): void {
    this.listener = listener;
  }

  unregisterDataChangeListener() {
    this.listener = null;
  }
}

 ```

## 接口说明

【详细可以参看[TitleBar.ets](https://gitee.com/smarthane/titlebar/blob/master/titlebar/src/main/ets/components/TitleBar.ets)文件描述】

````

@State TitleBar.Model = new TitleBar.Model();

// 标题栏 Padding
this.model.setTitleBarNormalPadding();
// 标题栏最小高度
this.model.setTitleBarMinHeight();
// 标题栏样式
this.model.setTitleBarStyle();
// 标题栏背景色
this.model.setTitleBarBackground();
// 标题栏底部线条颜色
this.model.setTitleBarBottomLineColor();
// 标题栏底部线条高度
this.model.setTitleBarBottomLineHeight();
// 标题栏渐变色
this.model.setTitleBarLinearGradient();
/***************************************************************
 * 中间标题
 ***************************************************************/
// 布局项间距
this.model.setTitleSpace();
// 标题名称
this.model.setTitleName();
// 标题颜色
this.model.setTitleFontColor();
// 标题大小
this.model.setTitleFontSize();
// 标题文字粗细
this.model.setTitleFontWeight();
// 标题文本超长时处理显示策略
this.model.setTitleTextOverflowType();
// 字体样式设置 Normal 标准 Italic 斜体
this.model.setTitleTextStyle();
// 子标题名称
this.model.setSubTitleName();
// 子标题颜色
this.model.setSubTitleFontColor();
// 子标题大小
this.model.setSubTitleFontSize();
// 子标题文字粗细
this.model.setSubTitleFontWeight();
// 子标题文本超长时处理显示策略
this.model.setSubTitleTextOverflowType();
// 子标题字体样式设置 Normal 标准 Italic 斜体
this.model.setSubTitleTextStyle();
// 标题栏点击回调
this.model.setOnTitleClickListener: () => void = () => {
};

/***************************************************************
 * 垂直跑马灯多文本动态显示效果
 ***************************************************************/
this.model.setMultipleTitleList();
// 垂直跑马灯多文本动态显示标题颜色
this.model.setMultipleTitleFontColor();
// 垂直跑马灯多文本动态显示标题大小
this.model.setMultipleTitleFontSize();
// 垂直跑马灯多文本动态显示标题文字粗细
this.model.setMultipleTitleFontWeight();
// 垂直跑马灯多文本动态显示标题文本超长时处理显示策略
this.model.setMultipleTitleTextOverflowType();
// 垂直跑马灯多文本动态显示字体样式设置 Normal 标准 Italic 斜体
this.model.setMultipleTitleTextStyle();
// 垂直跑马灯多文本动态显示文字显示位置
this.model.setMultipleTitleTextAlign();
// 垂直跑马灯多文本动态显示文字播放 Interval 间隔
this.model.setMultipleTitlePlayInterval();
// 垂直跑马灯多文本动态显示文字播放 Duration
this.model.setMultipleTitlePlayDuration();
// 垂直跑马灯多文本动态显示文字播放 ItemSpace
this.model.setMultipleTitlePlayItemSpace();

/***************************************************************
 * 左边按钮
 ***************************************************************/
// 左边布局项间距
this.model.setLeftTitleSpace();
// 左边标题多态样式 Normal
this.model.setLeftTitleStateNormalStyleColor();
// 左边标题多态样式 Pressed
this.model.setLeftTitleStatePressedStyleColor();
// 显示左边布局
this.model.setShowLeftSideLayout();
// 左边背景色
this.model.setLeftTitleBackground();

// 左边按钮标题名称
this.model.setLeftTitleName();
// 左边按钮标题颜色
this.model.setLeftTitleFontColor();
// 左边按钮标题文字大小
this.model.setLeftTitleFontSize();
// 左边按钮标题文字粗细
this.model.setLeftTitleFontWeight();

// 左边按钮图标
this.model.setLeftIcon();
// 左边按钮图标宽度
this.model.setLeftIconWidth();
// 左边按钮图标高度
this.model.setLeftIconHeight();
// 左边按钮图标 Padding
this.model.setLeftIconPadding();
// 左边按钮图标位置
this.model.setLeftIconGravity();

// 左边点击回调
setOnLeftClickListener: () => void = () => {
};
/***************************************************************
 * 右边按钮
 ***************************************************************/
// 右边布局项间距
this.model.setRightTitleSpace();
// 右边标题多态样式 Normal
this.model.setRightTitleStateNormalStyleColor();
// 右边标题多态样式 Pressed
this.model.setRightTitleStatePressedStyleColor();
// 显示右边布局
this.model.setShowRightSideLayout();
// 右边背景色
this.model.setRightTitleBackground();

// 右边按钮标题名称
this.model.setRightTitleName();
// 右边按钮标题颜色
this.model.setRightTitleFontColor();
// 右边按钮标题文字大小
this.model.setRightTitleFontSize();
// 右边按钮标题文字粗细
this.model.setRightTitleFontWeight();

// 右边按钮图标
this.model.setRightIcon();
// 右边按钮图标宽度
this.model.setRightIconWidth();
// 右边按钮图标高度
this.model.setRightIconHeight();
// 右边按钮图标 Padding
this.model.setRightIconPadding();
// 右边按钮图标位置
this.model.setRightIconGravity();

// 右边点击回调
setOnRightClickListener: () => void = () => {
};
````
## 目录结构
````
|---- TitleBar
|     |---- entry  # 示例代码文件夹
|     |---- titlebar  # TitleBar库文件夹
|           |---- index.ets  # 对外接口
|           |---- src
|                 |---- main
|                       |---- components
|                             |---- TitleBar.ets  # 自定义组件类
|     |---- README.md  # 安装使用方法                    
````
## 运行示例
本工程[entry]示例如需要运行在真机，需要替换build-profile.json5文件中的signingConfigs
[为应用/服务进行签名](https://developer.huawei.com/consumer/cn/doc/harmonyos-guides-V2/signing-0000001587684945-V2)


## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/smarthane/titlebar/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/material-dialogs/pulls) 。

## 开源协议
本项目基于 [ Apache License 2.0](https://gitee.com/smarthane/titlebar/blob/master/LICENSE) ，请自由地享受和参与开源。